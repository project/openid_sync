<?php


/**
* @file
* Admin callbacks for the OpenID Sync Server module.
*/

/**
 * Implementation of hook_admin_settings()
 */
function openid_sync_server_settings_form(&$form_state) {

  $form['openid_sync_server_openids'] = array(
    '#type' => 'textarea',
    '#title' => t('List of Admin OpenIDs'),
    '#default_value' => variable_get('openid_sync_server_openids', ''),
    '#size' => 20,
    '#rows' => 7,
    '#required' => TRUE,
    '#description' => t('Please enter one openID per line'),
  );

  return system_settings_form($form);
}